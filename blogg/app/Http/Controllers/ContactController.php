<?php

namespace App\Http\Controllers;

use App\Mail\MessageReceived;
use Illuminate\Support\Facades\Mail;
class ContactController extends Controller
{
    public function store(){
        $message = request()->validate([
            'name'=>'required',
            'email'=>'required|email',
            'subject'=>'required',
            'content'=>'required|min:3',
        ],[
            'name.required'=>__('I need your name')
        ]);
        Mail::to(env('MAIL_USER'))->queue(new MessageReceived($message));
        session()->flash('status',__('We received your message and we will respond in less than 24 hours.'));
        return back();
    }

}
