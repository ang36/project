<?php

namespace App\Http\Controllers;

use App\Project;

use App\Category;
use App\Events\ProjectSaved;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\SaveProjectRequest;

class ProjectController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except('index', 'show');
    }
    public function index(){
        return view('projects/project', [
            'projects'=>Project::with('category')->latest()->paginate()
        ]);
    }
    public function show(Project $project){
        return view('projects/show',[
            'project'=>$project
        ]);
    }
    public function create(){
        return view('projects/create',[
            'project'=> new Project,
            'categories'=>Category::pluck('name','id')
        ]);
        
    }
    public function store(SaveProjectRequest $request){
        $project = new Project($request->validated());
        $project->image = $request->file('image')->store('images');
        $project->save();
        ProjectSaved::dispatch($project);
        session()->flash('status', 'El proyecto fue creado con exito');
        return redirect()->route('project.index');
    }
    public function edit(Project $project){
        return view('projects/edit',[
            'project'=>$project,
            'categories'=>Category::pluck('name','id')
        ]);
    }
    public function update(Project $project, SaveProjectRequest $request){
        if ($request->hasFile('image')) {
            Storage::delete($project->image);
            $project = $project->fill($request->validated());
            $project->image = $request->file('image')->store('images');
            $project->save();
            ProjectSaved::dispatch($project);
        } else {
            $project->update(array_filter($request->validated()));
        }
        
        
        session()->flash('status', 'El proyecto fue actualizado con exito');
        return redirect()->route('project.show', $project);
    }
    public function destroy(Project $project){
        $project->delete();
        session()->flash('status', 'El proyecto fue eliminado con exito');
        return redirect()->route('project.index');
    }
    public function papelera(){
        return view('projects/papelera',[
            'deleteProjects'=> Project::onlyTrashed()->latest()->paginate()
        ]);
    }
    public function restore($projectUrl){
        $project = Project::onlyTrashed()->whereUrl($projectUrl)->firstOrFail();
        $project->restore();
        session()->flash('status', 'El proyecto fue restaurado con exito');
        return redirect()->route('project.index');
    }
    public function forceDelete($projectUrl){
        $project = Project::withTrashed()->whereUrl($projectUrl)->firstOrFail();
        Storage::delete($project->image);
        $project->forceDelete();
        session()->flash('status', 'El proyecto fue eliminado permanente con exito');
        return redirect()->route('project.index');

    }
}
