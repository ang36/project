<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;
    
    protected $guarded = [];

    public function getRouteKeyName(){
        return 'url';
    }
    public function category(){
        return $this->belongsTo('App\Category');
    }
}
