<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Project;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {
    return [
        'title'=>$faker->title,
        'description'=>$faker->text(80),
        'url'=>$faker->url
    ];
});
