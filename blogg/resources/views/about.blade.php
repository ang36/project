@extends('layaout')

@section('title', 'About')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-6 py-5">
				<h1 class="text-primary">@lang('About')</h1>
				<p class="lead text-secondary">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tenetur facere odio atque iusto consequatur facilis maxime deleniti labore assumenda blanditiis. Molestias optio, suscipit, accusantium porro repellat sed necessitatibus debitis quasi iure ipsa eum quam voluptas quod amet illum deserunt? Impedit quae dignissimos, doloribus architecto neque quis rem officiis cum rerum!</p>
			</div>
			<div class="col-12 col-12 col-lg-6">
				<img class="img-fluid" src="/img/about.png" alt="">
			</div>
		</div>
	</div>
@endsection