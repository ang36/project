@extends('layaout')

@section('title', 'Contacto')

@section('content')
	<div class="container">

	
		<div class="row">
			<div class="col-12 col-sm-10 col-lg-6 mx-auto">
				<form
					class="bg-white shadow rounded py-3 px-4"
					method="POST" 
					action="{{route('contact.store')}}">
					@csrf
					<h1 class="display-4 text-center text-primary">{{__('Contacto')}}</h1>
					<div class="form-group">
						<input
							class="form-control bg-light shadow-sm @error('name') is-invalid @else border-0 @enderror" 
							name="name" 
							placeholder="Name" 
							value="{{old('name')}}">
						
						@error('name')
							<span class="invalid-feedback" role="alert">
								<strong>
									{{$message}}	
								</strong>	
							</span>			
						@enderror
					</div>
					<div class="form-group">
						<input
							class="form-control bg-light shadow-sm @error('email') is-invalid @else border-0 @enderror" 
							type="text" 
							name="email" 
							placeholder="Email" 
							value="{{old('email')}}">

						@error('email')
							<span class="invalid-feedback" role="alert">
								<strong>
									{{$message}}	
								</strong>	
							</span>			
						@enderror
					</div>
					<div class="form-group">
						<input
							class="form-control bg-light shadow-sm @error('subject') is-invalid @else border-0 @enderror" 
							name="subject" 
							placeholder="Asunto" 
							value="{{old('subject')}}">
						
						@error('subject')
							<span class="invalid-feedback" role="alert">
								<strong>
									{{$message}}	
								</strong>	
							</span>			
						@enderror
					</div>
					<div class="form-group">
						<textarea
							class="form-control bg-light shadow-sm @error('content') is-invalid @else border-0 @enderror" 
							name="content" 
							placeholder="Contenido"
							rows="5">
							{{old('content')}}
						</textarea>

						@error('content')
							<span class="invalid-feedback" role="alert">
								<strong>
									{{$message}}	
								</strong>	
							</span>			
						@enderror
					</div>
					<button class="btn btn-primary btn-lg btn-block">@lang('Send')</button>
				</form>
			</div>
		</div>
		
	</div>

	
@endsection