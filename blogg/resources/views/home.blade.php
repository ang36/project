@extends('layaout')

@section('title', 'Home')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-12 col-lg-6">
			<h1 class="text-primary">@lang('Project management')</h1>
			<p class="lead text-secondary">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cumque quisquam molestiae illo at voluptatem unde eius porro aspernatur amet officiis minima aperiam explicabo vitae tempore, tenetur ducimus impedit neque assumenda, nisi perferendis quas, dignissimos nostrum ut! Quia perspiciatis harum numquam, eaque rerum minus quos maiores cumque optio quibusdam. Perspiciatis, itaque.</p>
		</div>
		<div class="col-12 col-lg-6">
			<img class="img-fluid mb-4" src="./img/project_team.png" alt="@lang('Project Project management')">
		</div>
	</div>
</div>
@endsection