<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>@yield('title')</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="{{mix('css/app.css')}}">
	<script src="{{mix('js/app.js')}}" defer></script>
	<style>
		
	</style>
</head>
<body>
	<div id="app" class="d-flex flex-column h-screen justify-content-between">
		<header>
			@include('partials/navHome')
			@include('partials/session_status')
		</header>
		<main class="py-4">
			@yield('content')
		</main>
		<footer class="bg-white text-center text-black-50 py-3 shadow">
			{{config('app.name')}} | Copyright @ {{date('Y')}}
		</footer>
	</div>
</body>
</html>