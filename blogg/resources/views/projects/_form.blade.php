@csrf
@if($project->image)
    <img class="card-img-top mb-3" style="height:400px; object-fit:cover" src="/storage/{{$project->image}}" alt="{{$project->title}}">
@endif
<div class="custom-file mb-3">
    <input type="file" name="image" class="custom-file-input  @error('image') is-invalid @else border-0 @enderror" id="customFile">
    <label class="custom-file-label" for="customFile">Choose file</label>

    @error('image')
        <span class="invalid-feedback" role="alert">
            <strong>
                {{$message}}
            </strong>
        </span>
    @enderror
</div>
<div class="form-group">
    <select 
        name="category_id" 
        class="form-control bg-light shadow-sm 
        @error('category_id') is-invalid @else border-0 @enderror">
        <option value="">Categoria del Proyecto</option>
        @foreach ($categories as $id => $name)
            <option 
                value="{{$id}}"
                @if($id == old('category_id',$project->category_id)) selected @endif>
                {{$name}}
            </option>
        @endforeach
    </select>

    @error('category_id')
        <span class="invalid-feedback" role="alert">
            <strong>
                {{$message}}
            </strong>
        </span>
    @enderror
</div>
<div class="form-group">
    <input 
        class="form-control bg-light shadow-sm 
        @error('title') is-invalid @else border-0 @enderror"  
        type="text" name="title" 
        placeholder="@lang('Project Title')" 
        value="{{old('title',$project->title)}}">

    @error('title')
        <span class="invalid-feedback" role="alert">
            <strong>
                {{$message}}
            </strong>
        </span>
    @enderror
</div>
<div class="form-group">
    <input 
        class="form-control bg-light shadow-sm 
        @error('url') is-invalid @else border-0 @enderror" 
        type="text" 
        name="url" 
        placeholder="@lang('Project URL')" 
        value="{{old('url',$project->url)}}">

    @error('url')
        <span class="invalid-feedback" role="alert">
            <strong>
                {{$message}}
            </strong>
        </span>
    @enderror
</div>
<div class="form-group">
    <textarea 
        class="form-control bg-light shadow-sm 
        @error('description') is-invalid @else  border-0 @enderror" 
        name="description" 
        placeholder="@lang('Project Description')"
        rows="5">
        {{old('description',$project->description)}}
    </textarea>

    @error('description')
        <span class="invalid-feedback" role="alert">
            <strong>
                {{$message}}
            </strong>
        </span>
    @enderror
</div>
<button class="btn btn-primary btn-lg btn-block">{{__($btnText)}}</button>
