@extends('layaout')

@section('title', __('Create Project'))

@section('content')
	<div class="container">
        <div class="row">
            <div class="col-12 col-sm-10 col-lg-6 mx-auto">
                <form 
                    class="bg-white shadow py-3 px-4" 
                    method="POST"
                    enctype="multipart/form-data"  
                    action="{{route('project.store')}}">

                    <h1 class="display-4 text-center pb-3">@lang('Create Project')</h1>
                    @include('projects/_form', ['btnText'=>'Save'])
                </form>
            </div>
        </div>
    </div>
@endsection
