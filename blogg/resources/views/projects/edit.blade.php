@extends('layaout')

@section('title', __('Projects | ') . $project->title)

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 col-sm-10 col-lg-6 mx-auto">
            <form
                class="bg-white shadow px-4" 
                method="POST"
                enctype="multipart/form-data"  
                action="{{route('project.update', $project)}}">
                @method('PUT')
                <h1 class="mb-4 text-center">@lang('Edit Project')</h1>

                @include('projects/_form', ['btnText'=>'Update'])
                <a 
                    class="btn btn-link btn-block"
                    href="{{route('project.index')}}">
                    @lang('Cancel')
                </a>
            </form>
        </div>
    </div>
    
</div>
@endsection