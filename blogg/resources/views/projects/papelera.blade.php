@extends('layaout')

@section('title', 'Portafolio')

@section('content')
	<div class="container">
		<div class="d-flex justify-content-center align-items-center mb-3">
            <h1 class="display-4 mb-0 text-primary">Papelera de Proyectos</h1>
		</div>
		<div class="d-flex flex-wrap justify-content-between aling-items-start">
			@foreach($deleteProjects as $project)
				<div class="card border-0 shadow-sm mt-4 mx-auto" style="width: 18rem;">
					@if($project->image)
						<img class="card-img-top" style="height:150px; object-fit:cover" src="/storage/{{$project->image}}" alt="{{$project->title}}">
					@endif
					<div class="card-body">
						<h5 class="card-title text-primary">
							{{$project->title}}
						</h5>
						<h6 class="card-subtitle text-secondary">{{$project->created_at->format('d/m/Y')}}</h6>
						<p class="card-text text-truncate mt-3">{{$project->description}}</p>
						<div class="d-flex justify-content-between align-items-center">
                            <form method="POST" action="{{route('project.restore',$project)}}">
								@csrf @method('PUT')
                                <button class="btn btn-info">Restaurar</button>
                            </form>
                            <form 
                                method="POST"
                                onsubmit="return confirm('Esta seguro de querer eliminar este projecto')" 
                                action="{{route('project.force-delete',$project)}}">
								@csrf @method('DELETE')
                                <button class="btn btn-danger">Eliminar</button>
                            </form>
							@if ($project->category_id)
								<a href="{{route('categories.show',$project->category)}}" class="badge badge-secondary">{{$project->category->name}}</a>
							@endif
						</div>
					</div>
				</div>
			@endforeach
			{{$deleteProjects->links()}}
		</div>
	</div>
@endsection
