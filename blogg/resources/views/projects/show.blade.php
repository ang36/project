@extends('layaout')

@section('title', 'Portafolio | ' . $project->title)

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-10 col-lg-8 mx-auto">
				@if($project->image)
					<img class="card-img-top" style="height:400px; object-fit:cover" src="/storage/{{$project->image}}" alt="{{$project->title}}">
				@endif
				<div class="bg-white p-5 shadow rounded">
					<h1 class="text-primary">@lang('Project')</h1>
					<hr>
					@if ($project->category_id)
						<a href="{{route('categories.show',$project->category)}}" class="badge badge-secondary mb-2">{{$project->category->name}}</a>
					@endif
					<p>{{$project->title}}</p>
					<p>{{$project->description}}</p>
					<hr>
					<div class="d-flex justify-content-between aling-items-center">
						<a href="{{route('project.index')}}">@lang('Regresar')</a>
						@auth
							<div class="btn-group">
								<a class="btn btn-primary" href="{{route('project.edit',$project)}}">@lang('Edit')</a>
								<a class="btn btn-danger" href="#" onclick="document.getElementById('delete_project').submit()">
									@lang('Destroy')
								</a>
							</div>
							<form
								id="delete_project"
								class="d-none" 
								method="POST" 
								action="{{route('project.destroy',$project)}}">
								@csrf @method('DELETE')
							</form>
						@endauth
					</div>
					
				</div>
			</div>
			
		</div>
		
	</div>
@endsection