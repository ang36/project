<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::view('/','home')->name('home');
Route::view('/quienes-somos','about')->name('about');
Route::view('/contacto','contact')->name('contact');
Route::get('portafolio/papelera', 'ProjectController@papelera')->name('project.papelera');
Route::resource('portafolio', 'ProjectController')
    ->names('project')
    ->parameters(['portafolio'=>'project']);
Route::put('portafolio/{projectUrl}/restore','ProjectController@restore')->name('project.restore');
Route::delete('portafolio/{projectUrl}/force-delete','ProjectController@forceDelete')->name('project.force-delete');
Route::get('categories/{category}', 'CategoryController@show')->name('categories.show');
Route::post('contact', 'ContactController@store')->name('contact.store');
Auth::routes();